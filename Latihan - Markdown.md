# Latihan - Markdown
Latihan untuk memahami teknologi markdown language
# K1

## K2

### K3


| header | header |
| ------ | ------ |
|     1   |    docker    |
|      2  |     container   |


> 
var nama
nama = "zainudin"
>

**ini bold**
<hr>

_ini italic_

***ini bold & italic***

<b> ini juga bold </b>

- [ ] check 1
- [ ] check 2

<img src="/uploads/273459170e98a4567b8752e916ab11d2/image.png" width=400>

<!---
![image](/uploads/273459170e98a4567b8752e916ab11d2/image.png)
--->

